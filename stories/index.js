import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { withDocs } from "storybook-readme";
import { boolean, withKnobs, text, object } from "@storybook/addon-knobs";

import { Button } from "../controls";
import ButtonReadMe from "../src/Controls/Button/README.md";
import { LoaderDots } from "../utils";
import LoaderDotsReadMe from "../src/Utils/LoaderDots/README.md";
import { ThemeProvider } from "../main";
import { Input } from "../forms";
import InputReadMe from "../src/Forms/Input/README.md";

storiesOf("Controls/Button", module)
  .addDecorator(withKnobs)
  .add(
    "Default",
    withDocs(ButtonReadMe, () => (
      <ThemeProvider>
        <Button
          disabled={boolean("disabled", false)}
          secondary={boolean("secondary", false)}
          loading={boolean("loading", false)}
          style={object("style", null)}
          onClick={action("onClick")}
        >
          {text("children", "Primary button")}
        </Button>
      </ThemeProvider>
    ))
  );

storiesOf("Utils/LoaderDots", module)
  .addDecorator(withKnobs)
  .add(
    "Default",
    withDocs(LoaderDotsReadMe, () => (
      <ThemeProvider>
        <LoaderDots color={text("color", null)} />
      </ThemeProvider>
    ))
  );

storiesOf("Forms/Input", module)
  .addDecorator(withKnobs)
  .add(
    "Default",
    withDocs(InputReadMe, () => (
      <ThemeProvider>
        <Input
          onChange={action("onChange")}
          value={text("value", "")}
          textArea={boolean("textArea", false)}
          placeholder={text("placeholder", "")}
        />
      </ThemeProvider>
    ))
  );
