const MODULES_DEFINITIONS = [
  {
    input: "lib/Forms.js",
    output: {
      name: "forms",
      file: "forms.js",
      format: "umd",
    },
  },
  {
    input: "lib/Controls.js",
    output: {
      name: "controls",
      file: "controls.js",
      format: "umd",
    },
  },
  {
    input: "lib/Utils.js",
    output: {
      name: "utils",
      file: "utils.js",
      format: "umd",
    },
  },
  {
    input: "lib/index.js",
    output: {
      name: "main",
      file: "main.js",
      format: "umd",
    },
  },
];

export default MODULES_DEFINITIONS;
