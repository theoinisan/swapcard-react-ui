import React from "react";
import renderer from "react-test-renderer";

import DEFAULT_THEME from "../../../src/DefaultTheme";
import { Input } from "../../../src/Forms/Input/Input";

test("Input should render", () => {
  const component = renderer.create(<Input theme={DEFAULT_THEME} />);
  let tree = component.toJSON();

  expect(tree).toMatchSnapshot();
});

test("Input should render text area", () => {
  const component = renderer.create(<Input theme={DEFAULT_THEME} textArea />);
  let tree = component.toJSON();

  expect(tree).toMatchSnapshot();
});
