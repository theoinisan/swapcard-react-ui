import React from "react";
import renderer from "react-test-renderer";

import { Button } from "../../../src/Controls/Button/Button";
import DEFAULT_THEME from "../../../src/DefaultTheme";

test("Button should render", () => {
  const component = renderer.create(
    <Button theme={DEFAULT_THEME}>This a test button</Button>
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test("Button should render with a loader", () => {
  const component = renderer.create(
    <Button theme={DEFAULT_THEME} loading>
      This a test button
    </Button>
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test("Button should render disabled", () => {
  const component = renderer.create(
    <Button theme={DEFAULT_THEME} disabled>
      This a test button
    </Button>
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test("Button should render with a submit type", () => {
  const component = renderer.create(
    <Button theme={DEFAULT_THEME} type="submit">
      This a test button
    </Button>
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
