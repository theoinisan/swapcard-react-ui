(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory(require('styled-jsx/style'), require('react'), require('prop-types')) :
	typeof define === 'function' && define.amd ? define(['styled-jsx/style', 'react', 'prop-types'], factory) :
	(global.utils = factory(global.JSXStyle,global.React,global.PropTypes));
}(this, (function (_JSXStyle,React,PropTypes) { 'use strict';

_JSXStyle = _JSXStyle && _JSXStyle.hasOwnProperty('default') ? _JSXStyle['default'] : _JSXStyle;
var React__default = 'default' in React ? React['default'] : React;
PropTypes = PropTypes && PropTypes.hasOwnProperty('default') ? PropTypes['default'] : PropTypes;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
  return typeof obj;
} : function (obj) {
  return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
};

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
    }
  }return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
  };
}();

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }return call && ((typeof call === "undefined" ? "undefined" : _typeof(call)) === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : _typeof(superClass)));
  }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var withTheme = function withTheme(WrappedComponent) {
  var ThemedComponent = function (_Component) {
    _inherits(ThemedComponent, _Component);

    function ThemedComponent() {
      _classCallCheck(this, ThemedComponent);

      return _possibleConstructorReturn(this, (ThemedComponent.__proto__ || Object.getPrototypeOf(ThemedComponent)).apply(this, arguments));
    }

    _createClass(ThemedComponent, [{
      key: "render",
      value: function render() {
        var theme = this.context.theme;

        return React__default.createElement(WrappedComponent, _extends({}, this.props, { theme: theme }));
      }
    }]);

    return ThemedComponent;
  }(React.Component);

  ThemedComponent.contextTypes = {
    theme: PropTypes.object.isRequired
  };
  return ThemedComponent;
};

var LoaderDots = function LoaderDots(props) {
  return React__default.createElement("div", {
    className: _JSXStyle.dynamic([["251815311", [props.color || props.theme.colors.secondary]]]) + " " + "LoaderDots"
  }, React__default.createElement("div", {
    className: _JSXStyle.dynamic([["251815311", [props.color || props.theme.colors.secondary]]]) + " " + "LoaderDots__dot"
  }), React__default.createElement("div", {
    className: _JSXStyle.dynamic([["251815311", [props.color || props.theme.colors.secondary]]]) + " " + "LoaderDots__dot"
  }), React__default.createElement("div", {
    className: _JSXStyle.dynamic([["251815311", [props.color || props.theme.colors.secondary]]]) + " " + "LoaderDots__dot"
  }), React__default.createElement(_JSXStyle, {
    styleId: "251815311",
    css: [".LoaderDots.__jsx-style-dynamic-selector{text-align:center;margin-top:-3px;min-width:41px;}", ".LoaderDots__dot.__jsx-style-dynamic-selector{width:7px;height:7px;background-color:" + (props.color || props.theme.colors.secondary) + ";border-radius:50%;display:inline-block;margin-right:10px;-webkit-animation:bounceDelay-__jsx-style-dynamic-selector 1s ease-in-out infinite both;animation:bounceDelay-__jsx-style-dynamic-selector 1s ease-in-out infinite both;-webkit-transform:scale(0);-ms-transform:scale(0);transform:scale(0);}", ".LoaderDots__dot.__jsx-style-dynamic-selector:last-child{margin-right:0;}", ".LoaderDots__dot.__jsx-style-dynamic-selector:first-child{-webkit-animation-delay:-0.32s !important;animation-delay:-0.32s !important;}", ".LoaderDots__dot.__jsx-style-dynamic-selector:nth-child(2){-webkit-animation-delay:-0.16s !important;animation-delay:-0.16s !important;}", "@-webkit-keyframes bounceDelay-__jsx-style-dynamic-selector{0%,\0 80%,\0 100%{-webkit-transform:scale(0);-ms-transform:scale(0);transform:scale(0);}40%{-webkit-transform:scale(1);-ms-transform:scale(1);transform:scale(1);}}", "@keyframes bounceDelay-__jsx-style-dynamic-selector{0%,\0 80%,\0 100%{-webkit-transform:scale(0);-ms-transform:scale(0);transform:scale(0);}40%{-webkit-transform:scale(1);-ms-transform:scale(1);transform:scale(1);}}"],
    dynamic: [props.color || props.theme.colors.secondary]
  }));
};

var LoaderDots$1 = withTheme(LoaderDots);

var Utils = { LoaderDots: LoaderDots$1 };

return Utils;

})));
