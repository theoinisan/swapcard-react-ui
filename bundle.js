'use strict';

var React = require('react');
var PropTypes = require('prop-types');

var classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

var createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();







var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};



var inherits = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
};











var possibleConstructorReturn = function (self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
};

var withTheme = function withTheme(WrappedComponent) {
  var ThemedComponent = function (_React$Component) {
    inherits(ThemedComponent, _React$Component);

    function ThemedComponent() {
      classCallCheck(this, ThemedComponent);
      return possibleConstructorReturn(this, (ThemedComponent.__proto__ || Object.getPrototypeOf(ThemedComponent)).apply(this, arguments));
    }

    createClass(ThemedComponent, [{
      key: "render",
      value: function render() {
        var theme = this.context.theme;

        return React.createElement(WrappedComponent, _extends({}, this.props, { theme: theme }));
      }
    }]);
    return ThemedComponent;
  }(React.Component);

  ThemedComponent.contextTypes = {
    theme: PropTypes.object.isRequired
  };
  return ThemedComponent;
};

var Input = function (_React$Component) {
  inherits(Input, _React$Component);

  function Input(props) {
    classCallCheck(this, Input);

    var _this = possibleConstructorReturn(this, (Input.__proto__ || Object.getPrototypeOf(Input)).call(this, props));

    _this._handleChange = function (e) {
      var value = e.target.value;
      var onChange = _this.props.onChange;

      _this.setState(function () {
        return {
          value: value
        };
      }, function () {
        if (typeof onChange === "function") {
          onChange(value);
        }
      });
    };

    _this._handleFocus = function () {
      _this.setState(function () {
        return {
          focus: true
        };
      });
    };

    _this._handleBlur = function () {
      _this.setState(function () {
        return {
          focus: false
        };
      });
    };

    _this.state = {
      value: props.value,
      focus: false
    };
    return _this;
  }

  createClass(Input, [{
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      this.setState(function () {
        return {
          value: nextProps.value
        };
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _props = this.props,
          textArea = _props.textArea,
          theme = _props.theme,
          placeholder = _props.placeholder;
      var _state = this.state,
          value = _state.value,
          focus = _state.focus;

      return React.createElement(
        "div",
        { className: "Input" },
        !textArea && React.createElement("input", {
          onChange: this._handleChange,
          value: value,
          placeholder: placeholder,
          onFocus: this._handleFocus,
          onBlur: this._handleBlur
        }),
        textArea && React.createElement("textarea", {
          onChange: this._handleChange,
          value: value,
          placeholder: placeholder,
          onFocus: this._handleFocus,
          onBlur: this._handleBlur
        }),
        React.createElement(
          "style",
          { jsx: true },
          "\n            .Input {\n              border-radius: " + theme.radius.sm + ";\n              border: solid 1px\n                " + (focus ? theme.colors.primary : theme.colors.borders) + ";\n              display: flex;\n              width: 100%;\n              align-items: center;\n              transition: 100ms;\n            }\n            .Input > input,\n            .Input > textarea {\n              font-family: " + theme.fonts.main + ";\n              color: " + theme.colors.textPrimary + ";\n              width: 100%;\n              border: none;\n              outline: 0;\n              padding: 10px;\n              margin: 0;\n              font-size: 14px;\n              background-color: transparent;\n            }\n          "
        )
      );
    }
  }]);
  return Input;
}(React.Component);

Input.defaultProps = {
  value: "",
  textArea: false,
  placeholder: ""
};


var Input$1 = withTheme(Input);

var Forms = {
  Input: Input$1
};

module.exports = Forms;
