(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory(require('styled-jsx/style'), require('react'), require('prop-types')) :
	typeof define === 'function' && define.amd ? define(['styled-jsx/style', 'react', 'prop-types'], factory) :
	(global.forms = factory(global.JSXStyle,global.React,global.PropTypes));
}(this, (function (_JSXStyle,React,PropTypes) { 'use strict';

_JSXStyle = _JSXStyle && _JSXStyle.hasOwnProperty('default') ? _JSXStyle['default'] : _JSXStyle;
var React__default = 'default' in React ? React['default'] : React;
PropTypes = PropTypes && PropTypes.hasOwnProperty('default') ? PropTypes['default'] : PropTypes;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
  return typeof obj;
} : function (obj) {
  return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
};

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _createClass$1 = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
    }
  }return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
  };
}();

function _classCallCheck$1(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn$1(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }return call && ((typeof call === "undefined" ? "undefined" : _typeof(call)) === "object" || typeof call === "function") ? call : self;
}

function _inherits$1(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : _typeof(superClass)));
  }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var withTheme = function withTheme(WrappedComponent) {
  var ThemedComponent = function (_Component) {
    _inherits$1(ThemedComponent, _Component);

    function ThemedComponent() {
      _classCallCheck$1(this, ThemedComponent);

      return _possibleConstructorReturn$1(this, (ThemedComponent.__proto__ || Object.getPrototypeOf(ThemedComponent)).apply(this, arguments));
    }

    _createClass$1(ThemedComponent, [{
      key: "render",
      value: function render() {
        var theme = this.context.theme;

        return React__default.createElement(WrappedComponent, _extends({}, this.props, { theme: theme }));
      }
    }]);

    return ThemedComponent;
  }(React.Component);

  ThemedComponent.contextTypes = {
    theme: PropTypes.object.isRequired
  };
  return ThemedComponent;
};

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
    }
  }return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
  };
}();

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }return call && ((typeof call === "undefined" ? "undefined" : _typeof(call)) === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : _typeof(superClass)));
  }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var Input = function (_React$Component) {
  _inherits(Input, _React$Component);

  function Input(props) {
    _classCallCheck(this, Input);

    var _this = _possibleConstructorReturn(this, (Input.__proto__ || Object.getPrototypeOf(Input)).call(this, props));

    _this._handleChange = function (e) {
      var value = e.target.value;
      var onChange = _this.props.onChange;

      _this.setState(function () {
        return {
          value: value
        };
      }, function () {
        if (typeof onChange === "function") {
          onChange(value);
        }
      });
    };

    _this._handleFocus = function () {
      _this.setState(function () {
        return {
          focus: true
        };
      });
    };

    _this._handleBlur = function () {
      _this.setState(function () {
        return {
          focus: false
        };
      });
    };

    _this.state = {
      value: props.value,
      focus: false
    };
    return _this;
  }

  _createClass(Input, [{
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      this.setState(function () {
        return {
          value: nextProps.value
        };
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _props = this.props,
          textArea = _props.textArea,
          theme = _props.theme,
          placeholder = _props.placeholder;
      var _state = this.state,
          value = _state.value,
          focus = _state.focus;

      return React__default.createElement("div", {
        className: _JSXStyle.dynamic([["808501088", [theme.radius.sm, focus ? theme.colors.primary : theme.colors.borders, theme.fonts.main, theme.colors.textPrimary]]]) + " " + "Input"
      }, !textArea && React__default.createElement("input", {
        onChange: this._handleChange,
        value: value,
        placeholder: placeholder,
        onFocus: this._handleFocus,
        onBlur: this._handleBlur,
        className: _JSXStyle.dynamic([["808501088", [theme.radius.sm, focus ? theme.colors.primary : theme.colors.borders, theme.fonts.main, theme.colors.textPrimary]]])
      }), textArea && React__default.createElement("textarea", {
        onChange: this._handleChange,
        value: value,
        placeholder: placeholder,
        onFocus: this._handleFocus,
        onBlur: this._handleBlur,
        className: _JSXStyle.dynamic([["808501088", [theme.radius.sm, focus ? theme.colors.primary : theme.colors.borders, theme.fonts.main, theme.colors.textPrimary]]])
      }), React__default.createElement(_JSXStyle, {
        styleId: "808501088",
        css: [".Input.__jsx-style-dynamic-selector{border-radius:" + theme.radius.sm + ";border:solid 1px " + (focus ? theme.colors.primary : theme.colors.borders) + ";display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;width:100%;-webkit-align-items:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-transition:100ms;transition:100ms;}", ".Input.__jsx-style-dynamic-selector>input.__jsx-style-dynamic-selector,.Input.__jsx-style-dynamic-selector>textarea.__jsx-style-dynamic-selector{font-family:" + theme.fonts.main + ";color:" + theme.colors.textPrimary + ";width:100%;border:none;outline:0;padding:10px;margin:0;font-size:14px;background-color:transparent;}"],
        dynamic: [theme.radius.sm, focus ? theme.colors.primary : theme.colors.borders, theme.fonts.main, theme.colors.textPrimary]
      }));
    }
  }]);

  return Input;
}(React__default.Component);

Input.defaultProps = {
  value: "",
  textArea: false,
  placeholder: ""
};

var Input$1 = withTheme(Input);

var Forms = {
  Input: Input$1
};

return Forms;

})));
