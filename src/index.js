// @flow
import ThemeProvider from "./ThemeProvider";
import withTheme from "./withTheme";

export default { ThemeProvider, withTheme };
