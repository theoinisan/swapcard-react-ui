// @flow
export type ThemeType = {
  fonts: {
    main: string,
  },
  colors: {
    primary: string,
    secondary: string,
    actionsPrimary: string,
    actionsSecondary: string,
    textPrimary: string,
    white: string,
    borders: string,
  },
  radius: {
    sm: string,
    lg: string,
    circle: string,
  },
  shadow: {
    sm: string,
    md: string,
  },
};
