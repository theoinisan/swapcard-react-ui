// @flow
import React from "react";

import withTheme from "../../withTheme";
import type { ThemeType } from "../../ThemeType";
import LoaderDots from "../../Utils/LoaderDots/LoaderDots";

type Props = {
  theme: ThemeType,
  children: any,
  onClick: (e: SyntheticEvent<HTMLButtonElement>) => void,
  type: "button" | "submit",
  disabled: boolean,
  secondary: boolean,
  loading: boolean,
  style: Object | null,
};

const Button = (props: Props) => (
  <button
    className={`Button ${
      props.disabled || props.loading ? "" : "Button--active"
    }`}
    onClick={props.onClick}
    type={props.type}
    style={props.style}
    disabled={props.disabled || props.loading}
  >
    <span className="Button__inner">{props.children}</span>
    <span className="Button__loader">
      <LoaderDots color={props.theme.colors.white} />
    </span>
    <style jsx>{`
      .Button {
        background-color: ${props.secondary
          ? props.theme.colors.actionsSecondary
          : props.theme.colors.actionsPrimary};
        color: ${props.theme.colors.white};
        border-radius: ${props.theme.radius.lg};
        font-family: ${props.theme.fonts.main};
        opacity: ${props.disabled || props.loading ? "0.7" : "1"};
        cursor: ${props.disabled || props.loading ? "auto" : "pointer"};
        border: none;
        font-weight: 600;
        letter-spacing: 1px;
        transition: 150ms;
        white-space: nowrap;
        padding: 5px 20px 6px;
        outline: 0;
        position: relative;
      }
      .Button--active:hover {
        box-shadow: ${props.theme.shadow.sm};
        transform: scale(1.01);
      }
      .Button--active:hover:active {
        box-shadow: none;
        animation: press 200ms ease-in-out;
      }
      .Button__inner {
        opacity: ${props.loading ? 0 : 1};
        transition: 150ms;
      }
      .Button__loader {
        opacity: ${props.loading ? 1 : 0};
        transition: 150ms;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
      }
      @keyframes press {
        from {
          transform: scale(1.01);
        }
        70% {
          transform: scale(0.97);
        }
        to {
          transform: scale(1);
        }
      }
    `}</style>
  </button>
);

Button.defaultProps = {
  type: "button",
  disabled: false,
  secondary: false,
  loading: false,
  style: null,
};

export { Button };

export default withTheme(Button);
