# Button

Basic button

```js
import { Button } from "swapcard-react-ui/controls";

<Button>Primary button</Button>;
```

<!-- STORY -->

| propName  | propType            | defaultValue | isRequired |
| --------- | ------------------- | ------------ | ---------- |
| children  | React.Node          | -            | +          |
| onClick   | func                | -            | -          |
| type      | "button" / "submit" | "button"     | -          |
| disabled  | boolean             | false        | -          |
| secondary | boolean             | false        | -          |
| loading   | boolean             | false        | -          |
| style     | Object / null       | null         | -          |
