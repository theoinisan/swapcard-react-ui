// @flow
import React, { Component } from "react";
import PropTypes from "prop-types";

const withTheme = (WrappedComponent: any) => {
  class ThemedComponent extends Component<any> {
    render() {
      const { theme } = this.context;
      return <WrappedComponent {...this.props} theme={theme} />;
    }
  }
  ThemedComponent.contextTypes = {
    theme: PropTypes.object.isRequired,
  };
  return ThemedComponent;
};

export default withTheme;
