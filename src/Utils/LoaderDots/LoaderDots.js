// @flow
import React from "react";

import withTheme from "../../withTheme";
import type { ThemeType } from "../../ThemeType";

type Props = {
  theme: ThemeType,
  color?: string,
};

const LoaderDots = (props: Props) => (
  <div className="LoaderDots">
    <div className="LoaderDots__dot" />
    <div className="LoaderDots__dot" />
    <div className="LoaderDots__dot" />
    <style jsx>
      {`
        .LoaderDots {
          text-align: center;
          margin-top: -3px;
          min-width: 41px;
        }
        .LoaderDots__dot {
          width: 7px;
          height: 7px;
          background-color: ${props.color || props.theme.colors.secondary};
          border-radius: 50%;
          display: inline-block;
          margin-right: 10px;
          animation: bounceDelay 1s ease-in-out infinite both;
          transform: scale(0);
        }
        .LoaderDots__dot:last-child {
          margin-right: 0;
        }
        .LoaderDots__dot:first-child {
          animation-delay: -0.32s !important;
        }
        .LoaderDots__dot:nth-child(2) {
          animation-delay: -0.16s !important;
        }
        @keyframes bounceDelay {
          0%,
          80%,
          100% {
            transform: scale(0);
          }
          40% {
            transform: scale(1);
          }
        }
      `}
    </style>
  </div>
);

export default withTheme(LoaderDots);
