# LoaderDots

Small loader (dots UI)

```js
import { LoaderDots } from "swapcard-react-ui/utils";

<LoaderDots />;
```

<!-- STORY -->

| propName | propType | defaultValue | isRequired |
| -------- | -------- | ------------ | ---------- |
| color    | string   | -            | -          |
