// @flow
import React from "react";
import PropTypes from "prop-types";

import type { ThemeType } from "./ThemeType";
import DEFAULT_THEME from "./DefaultTheme";

type Props = {
  theme: ThemeType,
  children: any,
};

class ThemeProvider extends React.Component<Props> {
  static defaultProps = {
    theme: DEFAULT_THEME,
  };
  getChildContext() {
    return {
      theme: this.props.theme,
    };
  }

  render() {
    return this.props.children;
  }
}

ThemeProvider.childContextTypes = {
  theme: PropTypes.object.isRequired,
};

export default ThemeProvider;
