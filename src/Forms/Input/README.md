# Input

Classic input

```js
import { Input } from "swapcard-react-ui/forms";

<Input
  value=""
  placeholder=""
  onChange={value => console.log(value)}
  textArea={false}
/>;
```

<!-- STORY -->

| propName    | propType  | defaultValue | isRequired |
| ----------- | --------- | ------------ | ---------- |
| value       | `string`  | `""`         | -          |
| onChange    | `func`    | `-`          | -          |
| textArea    | `boolean` | `false`      | -          |
| placeholder | `string`  | `""`         | -          |
