// @flow
import React from "react";

import withTheme from "../../withTheme";
import type { ThemeType } from "../../ThemeType";

type Props = {
  onChange?: (value: string) => void,
  theme: ThemeType,
  value: string,
  textArea: boolean,
  placeholder: string,
};

type State = {
  value: string,
  focus: boolean,
};

class Input extends React.Component<Props, State> {
  static defaultProps = {
    value: "",
    textArea: false,
    placeholder: "",
  };

  constructor(props: Props) {
    super(props);
    this.state = {
      value: props.value,
      focus: false,
    };
  }

  componentWillReceiveProps(nextProps: Props) {
    this.setState(() => ({
      value: nextProps.value,
    }));
  }

  _handleChange = (e: { target: { value: string } }): void => {
    const { value } = e.target;
    const { onChange } = this.props;
    this.setState(
      () => ({
        value,
      }),
      () => {
        if (typeof onChange === "function") {
          onChange(value);
        }
      }
    );
  };

  _handleFocus = (): void => {
    this.setState(() => ({
      focus: true,
    }));
  };

  _handleBlur = (): void => {
    this.setState(() => ({
      focus: false,
    }));
  };

  render() {
    const { textArea, theme, placeholder } = this.props;
    const { value, focus } = this.state;
    return (
      <div className="Input">
        {!textArea && (
          <input
            onChange={this._handleChange}
            value={value}
            placeholder={placeholder}
            onFocus={this._handleFocus}
            onBlur={this._handleBlur}
          />
        )}
        {textArea && (
          <textarea
            onChange={this._handleChange}
            value={value}
            placeholder={placeholder}
            onFocus={this._handleFocus}
            onBlur={this._handleBlur}
          />
        )}
        <style jsx>
          {`
            .Input {
              border-radius: ${theme.radius.sm};
              border: solid 1px
                ${focus ? theme.colors.primary : theme.colors.borders};
              display: flex;
              width: 100%;
              align-items: center;
              transition: 100ms;
            }
            .Input > input,
            .Input > textarea {
              font-family: ${theme.fonts.main};
              color: ${theme.colors.textPrimary};
              width: 100%;
              border: none;
              outline: 0;
              padding: 10px;
              margin: 0;
              font-size: 14px;
              background-color: transparent;
            }
          `}
        </style>
      </div>
    );
  }
}

export { Input };

export default withTheme(Input);
