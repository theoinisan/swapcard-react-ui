"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _LoaderDots = require("./Utils/LoaderDots/LoaderDots");

var _LoaderDots2 = _interopRequireDefault(_LoaderDots);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = { LoaderDots: _LoaderDots2.default };