"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});


var DEFAULT_THEME = {
  fonts: {
    main: "Open Sans, sans-serif"
  },
  colors: {
    primary: "#123456",
    secondary: "#58b570",
    actionsPrimary: "#58b570",
    actionsSecondary: "#678098",
    white: "#ffffff",
    textPrimary: "#123456",
    borders: "#dddddd"
  },
  radius: {
    sm: "3px",
    lg: "30px",
    circle: "50%"
  },
  shadow: {
    sm: "0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)",
    md: "0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)"
  }
};

exports.default = DEFAULT_THEME;