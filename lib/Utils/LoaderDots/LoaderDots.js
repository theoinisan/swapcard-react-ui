"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _style = require("styled-jsx/style");

var _style2 = _interopRequireDefault(_style);

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _withTheme = require("../../withTheme");

var _withTheme2 = _interopRequireDefault(_withTheme);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LoaderDots = function LoaderDots(props) {
  return _react2.default.createElement(
    "div",
    {
      className: _style2.default.dynamic([["251815311", [props.color || props.theme.colors.secondary]]]) + " " + "LoaderDots"
    },
    _react2.default.createElement("div", {
      className: _style2.default.dynamic([["251815311", [props.color || props.theme.colors.secondary]]]) + " " + "LoaderDots__dot"
    }),
    _react2.default.createElement("div", {
      className: _style2.default.dynamic([["251815311", [props.color || props.theme.colors.secondary]]]) + " " + "LoaderDots__dot"
    }),
    _react2.default.createElement("div", {
      className: _style2.default.dynamic([["251815311", [props.color || props.theme.colors.secondary]]]) + " " + "LoaderDots__dot"
    }),
    _react2.default.createElement(_style2.default, {
      styleId: "251815311",
      css: [".LoaderDots.__jsx-style-dynamic-selector{text-align:center;margin-top:-3px;min-width:41px;}", ".LoaderDots__dot.__jsx-style-dynamic-selector{width:7px;height:7px;background-color:" + (props.color || props.theme.colors.secondary) + ";border-radius:50%;display:inline-block;margin-right:10px;-webkit-animation:bounceDelay-__jsx-style-dynamic-selector 1s ease-in-out infinite both;animation:bounceDelay-__jsx-style-dynamic-selector 1s ease-in-out infinite both;-webkit-transform:scale(0);-ms-transform:scale(0);transform:scale(0);}", ".LoaderDots__dot.__jsx-style-dynamic-selector:last-child{margin-right:0;}", ".LoaderDots__dot.__jsx-style-dynamic-selector:first-child{-webkit-animation-delay:-0.32s !important;animation-delay:-0.32s !important;}", ".LoaderDots__dot.__jsx-style-dynamic-selector:nth-child(2){-webkit-animation-delay:-0.16s !important;animation-delay:-0.16s !important;}", "@-webkit-keyframes bounceDelay-__jsx-style-dynamic-selector{0%,\0 80%,\0 100%{-webkit-transform:scale(0);-ms-transform:scale(0);transform:scale(0);}40%{-webkit-transform:scale(1);-ms-transform:scale(1);transform:scale(1);}}", "@keyframes bounceDelay-__jsx-style-dynamic-selector{0%,\0 80%,\0 100%{-webkit-transform:scale(0);-ms-transform:scale(0);transform:scale(0);}40%{-webkit-transform:scale(1);-ms-transform:scale(1);transform:scale(1);}}"],
      dynamic: [props.color || props.theme.colors.secondary]
    })
  );
};

exports.default = (0, _withTheme2.default)(LoaderDots);