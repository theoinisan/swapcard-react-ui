"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ThemeProvider = require("./ThemeProvider");

var _ThemeProvider2 = _interopRequireDefault(_ThemeProvider);

var _withTheme = require("./withTheme");

var _withTheme2 = _interopRequireDefault(_withTheme);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = { ThemeProvider: _ThemeProvider2.default, withTheme: _withTheme2.default };