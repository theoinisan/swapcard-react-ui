"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Button = undefined;

var _style = require("styled-jsx/style");

var _style2 = _interopRequireDefault(_style);

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _withTheme = require("../../withTheme");

var _withTheme2 = _interopRequireDefault(_withTheme);

var _LoaderDots = require("../../Utils/LoaderDots/LoaderDots");

var _LoaderDots2 = _interopRequireDefault(_LoaderDots);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Button = function Button(props) {
  return _react2.default.createElement(
    "button",
    {
      onClick: props.onClick,
      type: props.type,
      style: props.style,
      disabled: props.disabled || props.loading,
      className: _style2.default.dynamic([["429183748", [props.secondary ? props.theme.colors.actionsSecondary : props.theme.colors.actionsPrimary, props.theme.colors.white, props.theme.radius.lg, props.theme.fonts.main, props.disabled || props.loading ? "0.7" : "1", props.disabled || props.loading ? "auto" : "pointer", props.theme.shadow.sm, props.loading ? 0 : 1, props.loading ? 1 : 0]]]) + " " + ("Button " + (props.disabled || props.loading ? "" : "Button--active"))
    },
    _react2.default.createElement(
      "span",
      {
        className: _style2.default.dynamic([["429183748", [props.secondary ? props.theme.colors.actionsSecondary : props.theme.colors.actionsPrimary, props.theme.colors.white, props.theme.radius.lg, props.theme.fonts.main, props.disabled || props.loading ? "0.7" : "1", props.disabled || props.loading ? "auto" : "pointer", props.theme.shadow.sm, props.loading ? 0 : 1, props.loading ? 1 : 0]]]) + " " + "Button__inner"
      },
      props.children
    ),
    _react2.default.createElement(
      "span",
      {
        className: _style2.default.dynamic([["429183748", [props.secondary ? props.theme.colors.actionsSecondary : props.theme.colors.actionsPrimary, props.theme.colors.white, props.theme.radius.lg, props.theme.fonts.main, props.disabled || props.loading ? "0.7" : "1", props.disabled || props.loading ? "auto" : "pointer", props.theme.shadow.sm, props.loading ? 0 : 1, props.loading ? 1 : 0]]]) + " " + "Button__loader"
      },
      _react2.default.createElement(_LoaderDots2.default, { color: props.theme.colors.white })
    ),
    _react2.default.createElement(_style2.default, {
      styleId: "429183748",
      css: [".Button.__jsx-style-dynamic-selector{background-color:" + (props.secondary ? props.theme.colors.actionsSecondary : props.theme.colors.actionsPrimary) + ";color:" + props.theme.colors.white + ";border-radius:" + props.theme.radius.lg + ";font-family:" + props.theme.fonts.main + ";opacity:" + (props.disabled || props.loading ? "0.7" : "1") + ";cursor:" + (props.disabled || props.loading ? "auto" : "pointer") + ";border:none;font-weight:600;-webkit-letter-spacing:1px;-moz-letter-spacing:1px;-ms-letter-spacing:1px;letter-spacing:1px;-webkit-transition:150ms;transition:150ms;white-space:nowrap;padding:5px 20px 6px;outline:0;position:relative;}", ".Button--active.__jsx-style-dynamic-selector:hover{box-shadow:" + props.theme.shadow.sm + ";-webkit-transform:scale(1.01);-ms-transform:scale(1.01);transform:scale(1.01);}", ".Button--active.__jsx-style-dynamic-selector:hover.__jsx-style-dynamic-selector:active{box-shadow:none;-webkit-animation:press-__jsx-style-dynamic-selector 200ms ease-in-out;animation:press-__jsx-style-dynamic-selector 200ms ease-in-out;}", ".Button__inner.__jsx-style-dynamic-selector{opacity:" + (props.loading ? 0 : 1) + ";-webkit-transition:150ms;transition:150ms;}", ".Button__loader.__jsx-style-dynamic-selector{opacity:" + (props.loading ? 1 : 0) + ";-webkit-transition:150ms;transition:150ms;position:absolute;top:50%;left:50%;-webkit-transform:translate(-50%,-50%);-ms-transform:translate(-50%,-50%);transform:translate(-50%,-50%);}", "@-webkit-keyframes press-__jsx-style-dynamic-selector{from{-webkit-transform:scale(1.01);-ms-transform:scale(1.01);transform:scale(1.01);}70%{-webkit-transform:scale(0.97);-ms-transform:scale(0.97);transform:scale(0.97);}to{-webkit-transform:scale(1);-ms-transform:scale(1);transform:scale(1);}}", "@keyframes press-__jsx-style-dynamic-selector{from{-webkit-transform:scale(1.01);-ms-transform:scale(1.01);transform:scale(1.01);}70%{-webkit-transform:scale(0.97);-ms-transform:scale(0.97);transform:scale(0.97);}to{-webkit-transform:scale(1);-ms-transform:scale(1);transform:scale(1);}}"],
      dynamic: [props.secondary ? props.theme.colors.actionsSecondary : props.theme.colors.actionsPrimary, props.theme.colors.white, props.theme.radius.lg, props.theme.fonts.main, props.disabled || props.loading ? "0.7" : "1", props.disabled || props.loading ? "auto" : "pointer", props.theme.shadow.sm, props.loading ? 0 : 1, props.loading ? 1 : 0]
    })
  );
};


Button.defaultProps = {
  type: "button",
  disabled: false,
  secondary: false,
  loading: false,
  style: null
};

exports.Button = Button;
exports.default = (0, _withTheme2.default)(Button);