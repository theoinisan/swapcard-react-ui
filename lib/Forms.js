"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Input = require("./Forms/Input/Input");

var _Input2 = _interopRequireDefault(_Input);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  Input: _Input2.default
};