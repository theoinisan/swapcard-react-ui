"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Input = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _style = require("styled-jsx/style");

var _style2 = _interopRequireDefault(_style);

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _withTheme = require("../../withTheme");

var _withTheme2 = _interopRequireDefault(_withTheme);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Input = function (_React$Component) {
  _inherits(Input, _React$Component);

  function Input(props) {
    _classCallCheck(this, Input);

    var _this = _possibleConstructorReturn(this, (Input.__proto__ || Object.getPrototypeOf(Input)).call(this, props));

    _this._handleChange = function (e) {
      var value = e.target.value;
      var onChange = _this.props.onChange;

      _this.setState(function () {
        return {
          value: value
        };
      }, function () {
        if (typeof onChange === "function") {
          onChange(value);
        }
      });
    };

    _this._handleFocus = function () {
      _this.setState(function () {
        return {
          focus: true
        };
      });
    };

    _this._handleBlur = function () {
      _this.setState(function () {
        return {
          focus: false
        };
      });
    };

    _this.state = {
      value: props.value,
      focus: false
    };
    return _this;
  }

  _createClass(Input, [{
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      this.setState(function () {
        return {
          value: nextProps.value
        };
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _props = this.props,
          textArea = _props.textArea,
          theme = _props.theme,
          placeholder = _props.placeholder;
      var _state = this.state,
          value = _state.value,
          focus = _state.focus;

      return _react2.default.createElement(
        "div",
        {
          className: _style2.default.dynamic([["808501088", [theme.radius.sm, focus ? theme.colors.primary : theme.colors.borders, theme.fonts.main, theme.colors.textPrimary]]]) + " " + "Input"
        },
        !textArea && _react2.default.createElement("input", {
          onChange: this._handleChange,
          value: value,
          placeholder: placeholder,
          onFocus: this._handleFocus,
          onBlur: this._handleBlur,
          className: _style2.default.dynamic([["808501088", [theme.radius.sm, focus ? theme.colors.primary : theme.colors.borders, theme.fonts.main, theme.colors.textPrimary]]])
        }),
        textArea && _react2.default.createElement("textarea", {
          onChange: this._handleChange,
          value: value,
          placeholder: placeholder,
          onFocus: this._handleFocus,
          onBlur: this._handleBlur,
          className: _style2.default.dynamic([["808501088", [theme.radius.sm, focus ? theme.colors.primary : theme.colors.borders, theme.fonts.main, theme.colors.textPrimary]]])
        }),
        _react2.default.createElement(_style2.default, {
          styleId: "808501088",
          css: [".Input.__jsx-style-dynamic-selector{border-radius:" + theme.radius.sm + ";border:solid 1px " + (focus ? theme.colors.primary : theme.colors.borders) + ";display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;width:100%;-webkit-align-items:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-transition:100ms;transition:100ms;}", ".Input.__jsx-style-dynamic-selector>input.__jsx-style-dynamic-selector,.Input.__jsx-style-dynamic-selector>textarea.__jsx-style-dynamic-selector{font-family:" + theme.fonts.main + ";color:" + theme.colors.textPrimary + ";width:100%;border:none;outline:0;padding:10px;margin:0;font-size:14px;background-color:transparent;}"],
          dynamic: [theme.radius.sm, focus ? theme.colors.primary : theme.colors.borders, theme.fonts.main, theme.colors.textPrimary]
        })
      );
    }
  }]);

  return Input;
}(_react2.default.Component);

Input.defaultProps = {
  value: "",
  textArea: false,
  placeholder: ""
};
exports.Input = Input;
exports.default = (0, _withTheme2.default)(Input);