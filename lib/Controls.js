"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Button = require("./Controls/Button/Button");

var _Button2 = _interopRequireDefault(_Button);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = { Button: _Button2.default };